<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add a global class to everything.
     *  We want it to come first, so stuff its filter does can be overridden.
     */
    array_unshift($classes, 'app');

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

/**
 * Render WordPress searchform using Blade
 */
add_filter('get_search_form', function () {
    return template('partials.searchform');
});

/**
 * Collect data for searchform.
 */
add_filter('sage/template/app/data', function ($data) {
    return $data + [
        'sf_action' => esc_url(home_url('/')),
        'sf_screen_reader_text' => _x('Search for:', 'label', 'sage'),
        'sf_placeholder' => esc_attr_x('Search &hellip;', 'placeholder', 'sage'),
        'sf_current_query' => get_search_query(),
        'sf_submit_text' => esc_attr_x('Search', 'submit button', 'sage'),
    ];
});

add_filter( 'wpp_custom_html', function ( $popular_posts, $instance ){
	//print_r($popular_posts);

  $arrayPost = array();

  foreach( $popular_posts as $popular_post ) {
    array_push( $arrayPost, $popular_post->id );
  }

  print_r($arrayPost);

  return $arrayPost;
}, 10, 2 );


add_filter( 'acf_the_content', function ( $content ){

    # Load the content
    $dom = new \DOMDocument();

    # With UTF-8 support
    # https://stackoverflow.com/questions/8218230/php-domdocument-loadhtml-not-encoding-utf-8-correctly
    $dom->loadHTML('<?xml encoding="utf-8" ?>' . $content);
    # Find all images
    $images = $dom->getElementsByTagName('img');
    //Create new wrapper div
    $new_div = $dom->createElement('div');
    $new_div->setAttribute('class','wrapper-list-icon d-flex');

    $new_div_img = $dom->createElement('div');
    $new_div_img->setAttribute('class','image-left align-items-center justify-content-center d-flex');

    $flag = false;


    //Find all images
    $images = $dom->getElementsByTagName('img');

    //Iterate though images
    foreach ($images AS $image) {
      if ($image->hasAttribute('class') && strstr($image->getAttribute('class'), 'alignleft')){
        $flag = true;

        $wrapperImg = $image->parentNode;
        //Clone our created div
        $new_div_clone = $new_div->cloneNode();
        $new_div_img_clone = $new_div_img->cloneNode();

        $image->setAttribute('class', 'img-fluid');

        //Replace image with this wrapper div
        $image->parentNode->replaceChild($new_div_img_clone,$image);
        //Append this image to wrapper div
        $new_div_img_clone->appendChild($image);
        $new_div_clone->appendChild($new_div_img_clone);

        //Replace image with this wrapper div
        $wrapperImg->parentNode->replaceChild($new_div_clone,$wrapperImg);
        //Append this image to wrapper div
        $new_div_clone->appendChild($wrapperImg);
      }elseif ($image->hasAttribute('class') && strstr($image->getAttribute('class'), 'aligncenter')){
        $flag = true;
        $image->setAttribute('class', 'd-block mx-auto image-center-content img-fluid-vertical');
      }else{
        $flag = true;
        $image->setAttribute('class', 'img-fluid');
      }
    }

    if($flag){
    # Strip DOCTYPE etc from output
      return str_replace(['<body>', '</body>'], '', $dom->saveHTML($dom->getElementsByTagName('body')->item(0)));
    }

    return $content;
}, 99 );



// add_filter( 'acf_the_content', function ( $content ){
//     // A regular expression of what to look for.
//    $pattern = '/(<img([^>]*)>)/i';
//    // What to replace it with. $1 refers to the content in the first 'capture group', in parentheses above
//    $replacement = '<div class="myphoto">$1</div>';
//
//    // run preg_replace() on the $content
//    $content = preg_replace( $pattern, $replacement, $content );
//
//    // return the processed content
//    return $content;
// });
