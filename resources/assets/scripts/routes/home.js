export default {
  init() {
    // JavaScript to be fired on the home page
    $('.partners').slick({
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 6,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            dots: false,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: false,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    //$('.partners').on('init', function(){
      //console.log('partners was init');
      //$('.partners').find('.slick-track').addClass('d-flex align-items-center');
    //});

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
    $('.partners').find('.slick-track').addClass('d-flex align-items-center');
  },
};
