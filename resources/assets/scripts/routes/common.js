import ScrollMagic from 'scrollmagic';

function setItemsConoce( reloadConoce = false ){
  var angulo = 0;
  var conoceItems = document.querySelectorAll("div.conoce-item");
  var anguloSumar = 360 / conoceItems.length;
  var circunferencia = '40vh';
  var vw = $( document ).width();
  if( vw > 767 && vw < 992){
    circunferencia = '30vh';
  }else if ( vw < 768 ) {
    circunferencia = '20vh';
  }

  var startIndex = 0;

  if (reloadConoce){
    startIndex = -1;
  }

  console.log('aca estoy');

  //$('.contenido-conoce').removeClass('active');

  $( ".conoce-item" ).each(function( index ) {
    var self = this;
    setTimeout(function () {
      $(self).css('-webkit-transform','translate3d('+circunferencia+', 0, 0) rotate('+angulo+'deg)');
      $(self).css('-moz-transform','translate3d('+circunferencia+', 0, 0) rotate('+angulo+'deg)');
      // add Opera, MS etc. variants
      $(self).css('transform','translate3d('+circunferencia+', 0, 0) rotate('+angulo+'deg)');
      $(self).data('angulo', angulo);


      if ( index > startIndex ){
        $(self).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
          function() {

            var pos = $(self).position();
            var anguloElement = $(self).data('angulo');

            var indice = index + 1;
            var posLeft = pos.left + 15 ;
            var posTop = pos.top - 5;

            $(self).data('top',posTop);
            $(self).data('left',posLeft);

            if( anguloElement > 90 && anguloElement < 271 ){
              posLeft = pos.left -200;
              $('.label-item-'+indice).css('text-align', 'right');
            }

            $('.label-item-'+indice).css('top', posTop + 'px');
            $('.label-item-'+indice).css('left', posLeft + 'px');

            $('.label-item-'+indice).addClass('active-view');

            if( indice == conoceItems.length && !reloadConoce ){
              $('.contenido-conoce-1').addClass('active');
            }

        });
      }else{
        var pos = $(self).position();

        var posLeft = pos.left + 15 ;
        var posTop = pos.top - 5;

        $(self).data('top',posTop);
        $(self).data('left',posLeft);

        var indice = index + 1;

        $('.label-item-'+indice).css('top', posTop + 'px');
        $('.label-item-'+indice).css('left', posLeft + 'px');
        $('.label-item-'+indice).addClass('active-view');
      }


      angulo = angulo + anguloSumar;

    }, index*1000);
  });

  var anguloActive = $('.conoce-item.active').data('angulo');

  $('.animacion-item').css('-webkit-transform','translate3d('+circunferencia+', 0, 0) rotate('+anguloActive+'deg)');
  $('.animacion-item').css('-moz-transform','translate3d('+circunferencia+', 0, 0) rotate('+anguloActive+'deg)');
  // add Opera, MS etc. variants
  $('.animacion-item').css('transform','translate3d('+circunferencia+', 0, 0) rotate('+anguloActive+'deg)');

}

export default {
  init() {
    // JavaScript to be fired on all pages

    var isMobile = false; //initiate as false
    // device detection
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw(n|u)|c55\/|capi|ccwa|cdm|cell|chtm|cldc|cmd|co(mp|nd)|craw|da(it|ll|ng)|dbte|dcs|devi|dica|dmob|do(c|p)o|ds(12|d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(|_)|g1 u|g560|gene|gf5|gmo|go(\.w|od)|gr(ad|un)|haie|hcit|hd(m|p|t)|hei|hi(pt|ta)|hp( i|ip)|hsc|ht(c(| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i(20|go|ma)|i230|iac( ||\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|[a-w])|libw|lynx|m1w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|mcr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|([1-8]|c))|phil|pire|pl(ay|uc)|pn2|po(ck|rt|se)|prox|psio|ptg|qaa|qc(07|12|21|32|60|[2-7]|i)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h|oo|p)|sdk\/|se(c(|0|1)|47|mc|nd|ri)|sgh|shar|sie(|m)|sk0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h|v|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl|tdg|tel(i|m)|tim|tmo|to(pl|sh)|ts(70|m|m3|m5)|tx9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas|your|zeto|zte/i.test(navigator.userAgent.substr(0,4))) {
        isMobile = true;
    }

    $(window).scroll(function() {
      var ventana_top = parseFloat($(window).scrollTop());
      //var servicios_top = parseFloat($('.servicios-footer').offset().top) - 400;
      //var snapchat_top = parseFloat($('.snapchat').offset().top) - 400;
      if (ventana_top > 100) {
        $('.banner').addClass("fijo");
        $('#primary-nav').removeClass("navbar-expand-lg");
      } else {
        $('.banner').removeClass("fijo");
        $('#navbarSupportedContent').removeClass("show");
        $('#primary-nav').addClass("navbar-expand-lg");
        $('.navbar-toggler').addClass("collapsed");
      }
      /*if (ventana_top > servicios_top) {
        $('.servicios-footer').find('.animated').addClass('aparecer');
      }
      if (ventana_top > snapchat_top) {
        $('.bg-left').find('.ghost').addClass('bounceInLeft');
        $('.bg-rigth').find('.ghost').addClass('bounceInRight');
      }*/
    });


		// init
    var controller = new ScrollMagic.Controller({
      globalSceneOptions: {
        triggerHook: 'onLeave',
      },
    });

    if (!isMobile) {
      console.log('para bloquear en movil');

      //get all slides
      var slides = document.querySelectorAll("section.section-item");

      //create scene for every slide
      for (var i=0; i<slides.length; i++) {
        new ScrollMagic.Scene({
          triggerElement: slides[i],
        })
        .setClassToggle("section.section-item", "zap")
        .setPin(slides[i])
        .addIndicators() //add indicators (requires plugin)
        .addTo(controller);
      }

      new ScrollMagic.Scene({
        triggerElement: '#conoce',
      })
      .setClassToggle("section.section-conoce", "zap")
      .setPin('#conoce')
      .addIndicators() //add indicators (requires plugin)
      .addTo(controller)
      .on('enter', function(){ setItemsConoce() } );
    }

    $( window ).resize( function(){setItemsConoce(true)} );

    $('.click-conoce').click(function(){
      var circunferencia = '40vh';

      var vw = $( document ).width();
      if( vw > 767 && vw < 992){
        circunferencia = '30vh';
      }else if ( vw < 768 ) {
        circunferencia = '20vh';
      }

      var itemId = $(this).data('id');
      var angulo = $('.conoce-item-'+itemId).data('angulo');

      var newBG = $(this).data('bg');

      $('.contenido-conoce').removeClass('active');
      $('.click-conoce').removeClass('active');


      $('.label-item-'+itemId).addClass('active');
      $('.conoce-item-'+itemId).addClass('active');

      $('.animacion-item').css('-webkit-transform','translate3d('+circunferencia+', 0, 0) rotate('+angulo+'deg)');
      $('.animacion-item').css('-moz-transform','translate3d('+circunferencia+', 0, 0) rotate('+angulo+'deg)');
      // add Opera, MS etc. variants
      $('.animacion-item').css('transform','translate3d('+circunferencia+', 0, 0) rotate('+angulo+'deg)');

      $(self).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
        function() {
          $('.contenido-conoce-'+itemId).addClass('active');
          var currentBG = $('#bg-conoce').data('bg');
          $('#bg-conoce').removeClass(currentBG);
          $('#bg-conoce').addClass(newBG);
          $('#bg-conoce').data('bg', newBG);
      });

    });


    $('.slider-centrado').slick({
      centerMode: true,
      centerPadding: '60px',
      slidesToShow: 2,
      dots: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1,
          },
        },
      ],
    });

    $('.load-more-blog').click(function(){
      var nextPage = $(this).data('next');
      var maxPages = $(this).data('max');
      $('.load-more-blog-wrapper').addClass('d-none');
      $.ajax({ type: "GET",
           url: "http://dev.arigatostudio.com/sweet/blog/page/"+nextPage,
           success : function(text)
           {
               var response= $(text).find('.inner-post').html(); console.log('respuesta'); $('.inner-post').append(response);
               $('.load-more-blog').data('next', nextPage * 1 + 1);
               if(nextPage == maxPages){
                 $('.load-more-blog-wrapper').addClass('d-none');
               }else{
                 $('.load-more-blog-wrapper').remveClass('d-none');
               }
           },
      });
    });

    $('.blog-card ').click(function(){
      window.location.href = $(this).data('url');
    });

    $( ".fondo-foto-3" ).append( '<div class="dots"><div class="inner-dot-outsie animated wafe infinite slower delay-4s"></div><div class="inner-dot-middle animated wafe infinite slow delay-3s"></div><div class="inner-dot-inside animated wafe-inner infinite slow"></div></div>' );
    $( ".fondo-foto-3" ).append( '<div class="dots-2"><div class="inner-dot-outsie animated wafe infinite slower delay-5s"></div><div class="inner-dot-middle-1 animated wafe infinite slow delay-4s"></div><div class="inner-dot-middle-2 animated wafe infinite slow delay-3s"></div><div class="inner-dot-inside animated wafe-inner infinite delay-2s"></div></div>' );

    $( ".fondo-foto-4" ).append( '<div class="dots"><div class="inner-dot-outsie animated wafe infinite slower delay-4s"></div><div class="inner-dot-middle animated wafe infinite slow delay-3s"></div><div class="inner-dot-inside animated wafe-inner infinite slow"></div></div>' );
    $( ".fondo-foto-4" ).append( '<div class="dots-2"><div class="inner-dot-outsie animated wafe infinite slower delay-5s"></div><div class="inner-dot-middle-1 animated wafe infinite slow delay-4s"></div><div class="inner-dot-middle-2 animated wafe infinite slow delay-3s"></div><div class="inner-dot-inside animated wafe-inner infinite delay-2s"></div></div>' );

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
