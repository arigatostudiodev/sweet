@extends('layouts.full')

@php
  $page = get_page_by_path('blog');
@endphp

@section('content')
  @if( have_rows('secciones',$page->ID) )
    @while ( have_rows('secciones',$page->ID) ) @php the_row(); @endphp

      @php $type = get_sub_field('tipo_de_seccion'); @endphp

      @if( $type == 'Slider' )
        @include('partials.sliders')
      @elseif( $type == 'Informativa' )
          @include('partials.informativa')
      @elseif( $type == 'Partners')
          @include('partials.partners')
      @elseif( $type == 'Hero')
          @include('partials.hero')
      @elseif( $type == 'Slider - Centrado' )
          @include('partials.slider-centrado')
      @elseif( $type == 'Blog' )
              @include('partials.blog')
      @else
          @include('partials.conoce')
      @endif

    @endwhile
  @endif
@endsection
