<section clas="mh-100vh">
  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 500 338">
 <defs>
 <radialGradient id="fondo" data-name="Degradado sin nombre 130" cx="273" cy="100" r="184" gradientUnits="userSpaceOnUse">
 <!-- <stop offset="0" stop-color="#fff" stop-opacity="0"/>
 <stop offset="0.11" stop-color="#7060ac" stop-opacity="0.20"/>
 <stop offset="0.3" stop-color="#7060ac" stop-opacity="0.5"/>
 <stop offset="0.48" stop-color="#7060ac" stop-opacity="0.7"/>
 <stop offset="0.64" stop-color="#7060ac" stop-opacity="0.8"/>
 <stop offset="0.70" stop-color="#7060ac" stop-opacity="0.9"/>
 <stop offset="0.77" stop-color="#7060ac" stop-opacity="1"/> -->
 <stop offset="0" stop-color="#7060ac" stop-opacity="0.7"/>
 </radialGradient>
 <linearGradient id="a" data-name="Degradado sin nombre 15" x1="130" y1="101.06" x2="387" y2="101.06" gradientUnits="userSpaceOnUse">
 <stop offset="0" stop-color="#2583a3" stop-opacity="0"/>
 <stop offset="0.01" stop-color="#2583a3" stop-opacity="0.05"/>
 <stop offset="0.03" stop-color="#2583a3" stop-opacity="0.27"/>
 <stop offset="0.06" stop-color="#2583a3" stop-opacity="0.47"/>
 <stop offset="0.1" stop-color="#2583a3" stop-opacity="0.64"/>
 <stop offset="0.13" stop-color="#2583a3" stop-opacity="0.77"/>
 <stop offset="0.18" stop-color="#2583a3" stop-opacity="0.87"/>
 <stop offset="0.23" stop-color="#2583a3" stop-opacity="0.95"/>
 <stop offset="0.31" stop-color="#2583a3" stop-opacity="0.99"/>
 <stop offset="0.52" stop-color="#2583a3"/>
 <stop offset="0.71" stop-color="#2583a3" stop-opacity="0.99"/>
 <stop offset="0.78" stop-color="#2583a3" stop-opacity="0.95"/>
 <stop offset="0.83" stop-color="#2583a3" stop-opacity="0.87"/>
 <stop offset="0.87" stop-color="#2583a3" stop-opacity="0.77"/>
 <stop offset="0.91" stop-color="#2583a3" stop-opacity="0.64"/>
 <stop offset="0.94" stop-color="#2583a3" stop-opacity="0.47"/>
 <stop offset="0.97" stop-color="#2583a3" stop-opacity="0.27"/>
 <stop offset="0.99" stop-color="#2583a3" stop-opacity="0.05"/>
 <stop offset="0.99" stop-color="#2583a3" stop-opacity="0"/>
 </linearGradient>

 <linearGradient id="b" x1="273.13" y1="34.69" x2="362.57" y2="274.63" xlink:href="#a"/>
 <linearGradient id="c" x1="378.89" y1="122.92" x2="214.51" y2="319.27" xlink:href="#a"/>
 <linearGradient id="d" x1="358.04" y1="255.12" x2="104.49" y2="219.26" xlink:href="#a"/>
 <linearGradient id="e" x1="250.96" y1="307.23" x2="120.97" y2="86.61" xlink:href="#a"/>
 <linearGradient id="f" x1="134.31" y1="244.26" x2="251.73" y2="16.7" xlink:href="#a"/>
 <linearGradient id="g" data-name="x2" x1="280.59" y1="43.75" x2="280.59" y2="28.75" gradientUnits="userSpaceOnUse">
 <stop offset="0" stop-color="#645ba3"/>
 <stop offset="0.12" stop-color="#5b64aa"/>
 <stop offset="0.32" stop-color="#417bbd"/>
 <stop offset="0.53" stop-color="#1e9cd7"/>
 <stop offset="0.99" stop-color="#35b7b2"/>
 </linearGradient>
 <linearGradient id="h" x1="378.51" y1="131.2" x2="378.51" y2="116.2" xlink:href="#g"/>
 <linearGradient id="i" x1="344.37" y1="273.98" x2="344.37" y2="258.98" xlink:href="#g"/>
 <linearGradient id="j" x1="209.39" y1="306.22" x2="209.39" y2="291.22" xlink:href="#g"/>
 <linearGradient id="k" x1="120.66" y1="220.44" x2="120.66" y2="205.44" xlink:href="#g"/>
 <linearGradient id="l" x1="146.6" y1="89.26" x2="146.6" y2="74.26" xlink:href="#g"/>
 <linearGradient id="m" data-name="Degradado sin nombre 86" x1="250" y1="249.74" x2="250" y2="231.68" gradientUnits="userSpaceOnUse">
 <stop offset="0" stop-color="#35b7b2"/>
 <stop offset="0.01" stop-color="#30a8ae"/>
 <stop offset="0.01" stop-color="#2b97a9"/>
 <stop offset="0.03" stop-color="#288ca5"/>
 <stop offset="0.04" stop-color="#2685a4"/>
 <stop offset="0.09" stop-color="#2583a3"/>
 <stop offset="0.55" stop-color="#2685a4"/>
 <stop offset="0.72" stop-color="#288ca5"/>
 <stop offset="0.84" stop-color="#2b97a9"/>
 <stop offset="0.94" stop-color="#30a8ae"/>
 <stop offset="0.99" stop-color="#35b7b2"/>
 </linearGradient>
 </defs>
 <title>circle menu</title>
 <image xlink:href="@asset('images/conoce/bg-1.jpg')" x="0" y="1" height="100%" width="100%" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_1.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" from="0" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </image>
 <image xlink:href="@asset('images/conoce/bg-2.jpg')" x="0" y="1" height="100%" width="100%" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_2.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" from="0" to="0" dur="0.3s" begin="btn_1.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </image>
 <image xlink:href="@asset('images/conoce/bg-1.jpg')" x="0" y="1" height="100%" width="100%" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_3.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" from="0" to="0" dur="0.3s" begin="btn_2.mouseover;btn_1.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </image>
 <image xlink:href="@asset('images/conoce/bg-2.jpg')" x="0" y="1" height="100%" width="100%" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_4.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" from="0" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_1.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </image>
 <image xlink:href="@asset('images/conoce/bg-1.jpg')" x="0" y="1" height="100%" width="100%" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_5.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" from="0" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_1.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </image>
 <image xlink:href="@asset('images/conoce/bg-2.jpg')" x="0" y="1" height="100%" width="100%" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_6.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" from="0" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_1.mouseover" fill="freeze" repeatCount="1"/>
 </image>
 <rect cx="0" cy="0" width="500" height="340" fill="url(#fondo)"></rect>
 <circle id="par_1" cx="250" cy="170" r="136.4" fill="none" stroke="#fff" stroke-width="1" stroke-miterlimit="10" stroke-linecap="round" stroke-dasharray="0 1000" transform="rotate(-75 250 170)">
 <animate attributeName="stroke-dasharray" begin="btn_1.mouseover" dur=".5s" repeatCount="1" to="0 1000" fill="freeze"/>
 <animate attributeName="stroke-dasharray" begin="btn_2.mouseover" dur=".5s" repeatCount="1" to="125 1000" fill="freeze"/>
 <animate attributeName="stroke-dasharray" begin="btn_3.mouseover" dur=".5s" repeatCount="1" to="280 1000" fill="freeze"/>
 <animate attributeName="stroke-dasharray" begin="btn_4.mouseover" dur=".5s" repeatCount="1" to="440 1000" fill="freeze"/>
 <animate attributeName="stroke-dasharray" begin="btn_5.mouseover" dur=".5s" repeatCount="1" to="570 1000" fill="freeze"/>
 <animate attributeName="stroke-dasharray" begin="btn_6.mouseover" dur=".5s" repeatCount="1" to="880 1000" fill="freeze"/>
 </circle>
 <circle id="par_1" cx="250" cy="170" r="136.4" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width=".2">
 </circle>
 <g id="over_gradient">
 <path opacity="0" d="M131.3,100.8a142.6,142.6,0,0,1,12.6-18.4c2.2-2.9,4.8-5.6,7.3-8.4l.9-1a9.4,9.4,0,0,0,1-1l2.1-1.9c1.3-1.3,2.6-2.7,4-3.9l4.3-3.6a18.4,18.4,0,0,1,2.2-1.7l2.2-1.7,4.6-3.3,4.7-3,2.3-1.5L182,50l4.9-2.7c1.6-.9,3.4-1.7,5-2.5l5.2-2.3a137.9,137.9,0,0,1,88.1-5.7,143.1,143.1,0,0,1,40.7,18.8,131.7,131.7,0,0,1,17.5,14.1c2.8,2.5,5.3,5.3,7.9,8l3.6,4.3c1.2,1.4,2.3,3,3.5,4.4A134.4,134.4,0,0,1,385.5,170a149.1,149.1,0,0,0-2.2-22.1,140.4,140.4,0,0,0-5.7-21.4,130.8,130.8,0,0,0-9-20.2,133.7,133.7,0,0,0-12.1-18.5,140.9,140.9,0,0,0-32.1-30,143.7,143.7,0,0,0-19.2-10.6,129.1,129.1,0,0,0-20.8-7.5,142,142,0,0,0-21.6-4.1l-5.5-.4H240.8a140.6,140.6,0,0,0-42.9,9.6c-1.7.6-3.4,1.4-5,2.1l-5.1,2.3L183,51.7,180.5,53l-2.3,1.5-4.7,2.9-4.6,3.2-2.2,1.6a18.4,18.4,0,0,0-2.2,1.7l-4.3,3.4A144.8,144.8,0,0,0,131.3,100.8Z" fill="url(#a)">
 <animate attributeName="opacity" begin="btn_1.mouseover" dur=".5s" repeatCount="1" to="1" fill="freeze"/>
 <animate attributeName="opacity" begin="btn_1.mouseout" dur=".5s" repeatCount="1" to="0" fill="freeze"/>
 </path>
 <path opacity="0" d="M273.9,35.3a133.8,133.8,0,0,1,21.6,5.4c3.6,1.1,7,2.5,10.4,3.9l1.3.5,1.3.6L311,47c1.7.8,3.4,1.5,5,2.4l4.9,2.8,2.4,1.4,2.4,1.5,4.6,3.1,4.5,3.4,2.3,1.7c.7.5,1.4,1.2,2.1,1.8l4.3,3.6,4,3.9,4,4a137.5,137.5,0,0,1,36.1,80.6,130.1,130.1,0,0,1,.1,22.5,128.3,128.3,0,0,1-3.5,22.2,133.1,133.1,0,0,1-7.1,21.3c-1.4,3.5-3.1,6.8-4.7,10.2l-2.8,4.9c-.9,1.6-2,3.2-2.9,4.8a134.7,134.7,0,0,1-68.9,54.6,161,161,0,0,0,20-9.8,134.4,134.4,0,0,0,18.1-12.8,144.6,144.6,0,0,0,15.8-15.5,135.3,135.3,0,0,0,13-17.8,141.4,141.4,0,0,0,16.9-40.5,134.6,134.6,0,0,0,3-43.8,144.7,144.7,0,0,0-3.7-21.7l-1.5-5.3-.4-1.3-.4-1.3-.9-2.6-.9-2.6-1-2.6-2.1-5.1a140.3,140.3,0,0,0-23.9-36.8c-1.2-1.5-2.6-2.7-3.8-4l-3.9-3.9-4.2-3.7a18.1,18.1,0,0,0-2.1-1.8l-2.1-1.7-4.4-3.4-4.6-3.2L322.5,55l-2.4-1.5-4.8-2.8A144.4,144.4,0,0,0,273.9,35.3Z" fill="url(#b)">
 <animate attributeName="opacity" begin="btn_2.mouseover" dur=".5s" repeatCount="1" to="1" fill="freeze"/>
 <animate attributeName="opacity" begin="btn_2.mouseout" dur=".5s" repeatCount="1" to="0" fill="freeze"/>
 </path>
 <path opacity="0" d="M378.7,123.9a144.6,144.6,0,0,1,6,21.4,106.8,106.8,0,0,1,1.8,11.1v1.3c.1.5.1,1,.2,1.4l.2,2.8c.1,1.9.3,3.7.3,5.6v8.4l-.2,2.8-.4,5.6-.7,5.5-.3,2.8-.6,2.8-1,5.5c-.4,1.8-.9,3.6-1.4,5.4s-.9,3.7-1.5,5.4A137.4,137.4,0,0,1,328.9,283a128.7,128.7,0,0,1-19.4,11.2,141,141,0,0,1-21.1,8,135.8,135.8,0,0,1-22,4.3c-3.7.5-7.4.7-11.2.9h-5.6l-5.6-.2a134.5,134.5,0,0,1-81.5-32.8,151.6,151.6,0,0,0,18.3,12.5,136.1,136.1,0,0,0,20.1,9.4,129.7,129.7,0,0,0,21.3,6.1,133.5,133.5,0,0,0,21.9,2.5,139.6,139.6,0,0,0,43.6-5.4,127.6,127.6,0,0,0,20.5-7.9,116.4,116.4,0,0,0,19-11.1,142.2,142.2,0,0,0,17.1-13.9l3.8-3.9,1-1,.9-1.1,1.8-2,1.8-2.1,1.8-2.1,3.4-4.4A140.4,140.4,0,0,0,379,211a46.4,46.4,0,0,0,1.6-5.3c.5-1.7,1-3.5,1.4-5.3l1.2-5.4a25.6,25.6,0,0,0,.5-2.7l.4-2.7.8-5.5.5-5.5.2-2.8a24.4,24.4,0,0,0,.1-2.7v-5.6A143.7,143.7,0,0,0,378.7,123.9Z" fill="url(#c)">
 <animate attributeName="opacity" begin="btn_3.mouseover" dur=".5s" repeatCount="1" to="1" fill="freeze"/>
 <animate attributeName="opacity" begin="btn_3.mouseout" dur=".5s" repeatCount="1" to="0" fill="freeze"/>
 </path>
 <path opacity="0" d="M357.1,255.5a136.3,136.3,0,0,1-15,16.4c-2.6,2.6-5.6,4.9-8.4,7.3l-1.1.9-1.2.8-2.2,1.7c-1.5,1-3,2.2-4.6,3.2l-4.7,3-2.4,1.4-2.5,1.3-4.9,2.7-5.1,2.3-2.6,1.2-2.6,1-5.2,2-5.4,1.7c-1.8.5-3.5,1.2-5.4,1.6a138.1,138.1,0,0,1-88.1-6.7,144.6,144.6,0,0,1-19.8-10.6,139.9,139.9,0,0,1-33.2-30.1c-2.3-2.9-4.5-6-6.6-9.1l-3-4.7c-1-1.6-1.9-3.3-2.8-4.9a134.2,134.2,0,0,1-15.2-86.5,164.9,164.9,0,0,0-.9,22.2,136.2,136.2,0,0,0,2.6,22,131.5,131.5,0,0,0,6.1,21.2,135,135,0,0,0,9.4,20A140.1,140.1,0,0,0,159.9,271a132.5,132.5,0,0,0,17.6,13.2,116.1,116.1,0,0,0,19.4,10.3,141.7,141.7,0,0,0,20.8,7.1l5.4,1.2,1.4.3,1.3.2,2.7.5,2.8.5,2.7.3,5.5.6a137.6,137.6,0,0,0,43.8-3.5,51.8,51.8,0,0,0,5.3-1.4l5.3-1.6,5.2-1.8,2.6-1,2.5-1.1,5.1-2.2,4.9-2.5,2.5-1.3,2.4-1.4,4.7-2.8A142.3,142.3,0,0,0,357.1,255.5Z" fill="url(#d)">
 <animate attributeName="opacity" begin="btn_4.mouseover" dur=".5s" repeatCount="1" to="1" fill="freeze"/>
 <animate attributeName="opacity" begin="btn_4.mouseout" dur=".5s" repeatCount="1" to="0" fill="freeze"/>
 </path>
 <path opacity="0" d="M250.1,306.7a127.5,127.5,0,0,1-22.2-1.5c-3.7-.4-7.3-1.3-11-2l-1.3-.3-1.4-.4-2.7-.7-5.4-1.6-5.2-1.8-2.7-1-2.5-1.1-5.2-2.2-5-2.6-2.5-1.2-2.4-1.4-4.8-2.9-4.7-3.1c-1.5-1.1-3.1-2-4.6-3.2a137.6,137.6,0,0,1-49.6-73.1,121,121,0,0,1-4.1-22.1,141,141,0,0,1-.5-22.5,138.3,138.3,0,0,1,3.3-22.2c.8-3.7,1.9-7.3,2.9-10.9l1.9-5.3c.6-1.7,1.4-3.5,2.1-5.2a133.5,133.5,0,0,1,58.2-65.8,144.1,144.1,0,0,0-17.9,13.1,129.3,129.3,0,0,0-15.6,15.8,136.7,136.7,0,0,0-12.9,18,139.3,139.3,0,0,0-19.2,62.7,147,147,0,0,0,.5,22,132.7,132.7,0,0,0,4.1,21.6,150.3,150.3,0,0,0,7.5,20.7l2.4,5,.6,1.2.7,1.2,1.3,2.4,1.3,2.4,1.5,2.4,2.9,4.6a140.9,140.9,0,0,0,30,32.1c1.4,1.2,3,2.2,4.5,3.3s2.9,2.2,4.5,3.2l4.7,2.8,2.4,1.5,2.4,1.3,4.9,2.6,5,2.2,2.6,1.2,2.5,1,5.2,1.9A139.9,139.9,0,0,0,250.1,306.7Z" fill="url(#e)">
 <animate attributeName="opacity" begin="btn_5.mouseover" dur=".5s" repeatCount="1" to="1" fill="freeze"/>
 <animate attributeName="opacity" begin="btn_5.mouseout" dur=".5s" repeatCount="1" to="0" fill="freeze"/>
 </path>
 <path opacity="0" d="M134.3,243.3a142.3,142.3,0,0,1-10.6-19.6c-1.6-3.4-2.8-6.9-4.1-10.4l-.5-1.3c-.1-.5-.3-.9-.4-1.3l-.8-2.7c-.5-1.8-1.1-3.6-1.5-5.4l-1.3-5.5-.6-2.7-.4-2.8-.9-5.5-.5-5.6-.2-2.8a25.3,25.3,0,0,1-.1-2.8l-.2-5.6c0-1.9.1-3.7.1-5.6s.1-3.7.3-5.6a137.9,137.9,0,0,1,35.4-81,133,133,0,0,1,16.6-15.2,143.5,143.5,0,0,1,18.7-12.3,140.1,140.1,0,0,1,20.6-9.1c3.5-1.3,7.1-2.3,10.7-3.3l5.5-1.3,5.5-1a136.8,136.8,0,0,1,44.7-.2,135,135,0,0,1,42,14.4,157.8,157.8,0,0,0-20.6-8.2A134.4,134.4,0,0,0,226,37.2,139.5,139.5,0,0,0,184.6,52a129.4,129.4,0,0,0-18.3,12.2,126.6,126.6,0,0,0-16.1,15,141,141,0,0,0-13.5,17.3l-2.9,4.7-.8,1.1-.6,1.3L131,106l-1.3,2.4-1.2,2.5-2.4,4.9a142,142,0,0,0-11.2,42.5c-.2,1.8-.2,3.7-.4,5.5s-.2,3.7-.2,5.5v5.6c.1.9,0,1.8.1,2.7l.2,2.8.4,5.5.8,5.5.4,2.7a25.6,25.6,0,0,0,.5,2.7l1.1,5.5A142.8,142.8,0,0,0,134.3,243.3Z" fill="url(#f)">
 <animate attributeName="opacity" begin="btn_6.mouseover" dur=".5s" repeatCount="1" to="1" fill="freeze"/>
 <animate attributeName="opacity" begin="btn_6.mouseout" dur=".5s" repeatCount="1" to="0" fill="freeze"/>
 </path>
 </g>
 <g id="btn">
 <circle id="btn_1" cx="280.6" cy="36.2" r="7.5" fill="#fff" onmouseover="evt.target.setAttribute('fill', 'url(#g)');" onmouseout="evt.target.setAttribute('fill', '#fff');">
 <animate attributeType="XML" attributeName="r" to="10" dur=".5s" begin="btn_1.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeType="XML" attributeName="r" to="7.5" dur=".5s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </circle>
 <circle id="btn_2" cx="378.5" cy="123.7" r="7.5" fill="#fff" onmouseover="evt.target.setAttribute('fill', 'url(#h)');" onmouseout="evt.target.setAttribute('fill', '#fff');">
 <animate attributeType="XML" attributeName="r" to="10" dur=".5s" begin="btn_2.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeType="XML" attributeName="r" to="7.5" dur=".5s" begin="btn_1.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </circle>
 <circle id="btn_3" cx="344.4" cy="266.5" r="7.5" fill="#fff" onmouseover="evt.target.setAttribute('fill', 'url(#i)');" onmouseout="evt.target.setAttribute('fill', '#fff');">
 <animate attributeType="XML" attributeName="r" to="10" dur=".5s" begin="btn_3.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeType="XML" attributeName="r" to="7.5" dur=".5s" begin="btn_2.mouseover;btn_1.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </circle>
 <circle id="btn_4" cx="209.4" cy="298.7" r="7.5" fill="#fff" onmouseover="evt.target.setAttribute('fill', 'url(#j)');" onmouseout="evt.target.setAttribute('fill', '#fff');">
 <animate attributeType="XML" attributeName="r" to="10" dur=".5s" begin="btn_4.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeType="XML" attributeName="r" to="7.5" dur=".5s" begin="btn_2.mouseover;btn_3.mouseover;btn_1.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </circle>
 <circle id="btn_5" cx="120.7" cy="212.9" r="7.5" fill="#fff" onmouseover="evt.target.setAttribute('fill', 'url(#k)');" onmouseout="evt.target.setAttribute('fill', '#fff');">
 <animate attributeType="XML" attributeName="r" to="10" dur=".5s" begin="btn_5.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeType="XML" attributeName="r" to="7.5" dur=".5s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_1.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </circle>
 <circle id="btn_6" cx="146.6" cy="81.8" r="7.5" fill="#fff" onmouseover="evt.target.setAttribute('fill', 'url(#l)');" onmouseout="evt.target.setAttribute('fill', '#fff');">
 <animate attributeType="XML" attributeName="r" to="10" dur=".5s" begin="btn_6.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeType="XML" attributeName="r" to="7.5" dur=".5s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_1.mouseover" fill="freeze" repeatCount="1"/>
 </circle>
 </g>
 <g id="btn_titles">
 <text transform="translate(233.2 15.8) scale(1.04 1)" font-size="11.6" fill="#fff" opacity=".5" font-family="Sailec">
 Single Customer View (SCV)
 <animate attributeName="opacity" from=".5" to="1" dur="0.7s" begin="btn_1.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" from="1" to=".5" dur="0.7s" begin="btn_1.mouseout" fill="freeze" repeatCount="1"/>
 </text>
 <text transform="translate(397.2 134.5) scale(1.04 1)" font-size="11.6" fill="#fff" opacity=".5" font-family="Sailec">
 RFM
 <animate attributeName="opacity" from=".5" to="1" dur="0.7s" begin="btn_2.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" from="1" to=".5" dur="0.7s" begin="btn_2.mouseout" fill="freeze" repeatCount="1"/>
 </text>
 <text transform="translate(361 284.4) scale(1.04 1)" font-size="11.6" fill="#fff" opacity=".5" font-family="Sailec">
 RFPD
 <animate attributeName="opacity" from=".5" to="1" dur="0.7s" begin="btn_3.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" from="1" to=".5" dur="0.7s" begin="btn_3.mouseout" fill="freeze" repeatCount="1"/>
 </text>
 <text transform="translate(154.4 326.4) scale(1.04 1)" font-size="11.6" fill="#fff" opacity=".5" font-family="Sailec">
 Real Time (RT)
 <animate attributeName="opacity" from=".5" to="1" dur="0.7s" begin="btn_4.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" from="1" to=".5" dur="0.7s" begin="btn_4.mouseout" fill="freeze" repeatCount="1"/>
 </text>
 <text transform="translate(3.5 233.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity=".5" font-family="Sailec">
 Specific clientes (SC)
 <animate attributeName="opacity" from=".5" to="1" dur="0.7s" begin="btn_5.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" from="1" to=".5" dur="0.7s" begin="btn_5.mouseout" fill="freeze" repeatCount="1"/>
 </text>
 <text transform="translate(34.9 78.3) scale(1.04 1)" font-size="11.6" fill="#fff" opacity=".5" font-family="Sailec">
 User Journey (UJ)
 <animate attributeName="opacity" from=".5" to="1" dur="0.7s" begin="btn_6.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" from="1" to=".5" dur="0.7s" begin="btn_6.mouseout" fill="freeze" repeatCount="1"/>
 </text>
 </g>

 <g id="content_1">
 <text class="cont_1" transform="translate(183 137.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 1
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_1.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 157.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 1
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_1.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 167.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 1
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_1.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 177.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 1
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_1.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 187.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 1
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_1.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <a xlink:href="http://www.google.com">
 <rect id="link_1" x="231.5" y="232.7" width="37.1" height="16.06" rx="8" ry="8" fill="#fff" fill-opacity="0" stroke-miterlimit="10" stroke-width="2" stroke="url(#m)" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_1.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </rect>
 <line id="giro1" x1="248.9" y1="237.1" x2="248.9" y2="244.3" fill="none" stroke="#fff" stroke-miterlimit="10" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_1.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="1s" repeatCount="indefinite" begin="link_1.mouseover" from="0 248.89999389648438 240.70000457763672" to="360 248.89999389648438 240.70000457763672"/>
 </line>
 <line id="giro2" x1="252.6" y1="240.7" x2="245.2" y2="240.7" fill="none" stroke="#fff" stroke-miterlimit="10" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_1.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 <animateTransform attributeName="transform" attributeType="XML" type="rotate" begin="link_1.mouseover" end="link_1.mouseout" dur="1s" repeatCount="1" to="360 248.9000015258789 240.6999969482422"/>
 </line>
 </a>
 </g>
 <g id="content_2">
 <text class="cont_1" transform="translate(183 137.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 2
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_2.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 157.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 2
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_2.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 167.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 2
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_2.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 177.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 2
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_2.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 187.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 2
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_2.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>

 <a xlink:href="http://www.google.com">
 <rect x="231.5" y="232.7" width="37.1" height="16.06" rx="8" ry="8" fill="#fff" fill-opacity="0" stroke-miterlimit="10" stroke-width="2" stroke="url(#m)" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_2.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </rect>
 <line id="giro3" x1="248.9" y1="237.1" x2="248.9" y2="244.3" fill="none" stroke="#fff" stroke-miterlimit="10" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_2.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </line>
 <line id="giro4" x1="252.6" y1="240.7" x2="245.2" y2="240.7" fill="none" stroke="#fff" stroke-miterlimit="10" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_2.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </line>
 </a>
 </g>
 <g id="content_3">
 <text class="cont_1" transform="translate(183 137.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 3
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_3.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_2.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 157.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 3
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_3.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_2.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 167.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 3
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_3.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_2.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 177.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 3
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_3.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_2.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 187.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 3
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_3.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_2.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <a xlink:href="http://www.google.com">
 <rect x="231.5" y="232.7" width="37.1" height="16.06" rx="8" ry="8" fill="#fff" fill-opacity="0" stroke-miterlimit="10" stroke-width="2" stroke="url(#m)" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_3.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_2.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </rect>
 <line id="giro5" x1="248.9" y1="237.1" x2="248.9" y2="244.3" fill="none" stroke="#fff" stroke-miterlimit="10" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_3.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_2.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </line>
 <line id="giro6" x1="252.6" y1="240.7" x2="245.2" y2="240.7" fill="none" stroke="#fff" stroke-miterlimit="10" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_3.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_1.mouseover;btn_2.mouseover;btn_4.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </line>
 </a>

 </g>
 <g id="content_4">
 <text class="cont_1" transform="translate(183 137.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 4
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_4.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_1.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 157.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 4
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_4.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_1.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 167.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 4
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_4.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_1.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 177.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 4
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_4.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_1.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 187.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 4
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_4.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_1.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <a xlink:href="http://www.google.com">
 <rect x="231.5" y="232.7" width="37.1" height="16.06" rx="8" ry="8" fill="#fff" fill-opacity="0" stroke-miterlimit="10" stroke-width="2" stroke="url(#m)" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_4.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_1.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </rect>
 <line id="giro7" x1="248.9" y1="237.1" x2="248.9" y2="244.3" fill="none" stroke="#fff" stroke-miterlimit="10" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_4.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_1.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </line>
 <line id="giro8" x1="252.6" y1="240.7" x2="245.2" y2="240.7" fill="none" stroke="#fff" stroke-miterlimit="10" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_4.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_1.mouseover;btn_5.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </line>
 </a>
 </g>
 <g id="content_5">
 <text class="cont_1" transform="translate(183 137.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 5
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_5.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_1.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 157.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 5
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_5.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_1.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 167.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 5
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_5.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_1.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 177.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 5
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_5.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_1.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 187.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 5
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_5.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_1.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <a xlink:href="http://www.google.com">
 <rect x="231.5" y="232.7" width="37.1" height="16.06" rx="8" ry="8" fill="#fff" fill-opacity="0" stroke-miterlimit="10" stroke-width="2" stroke="url(#m)" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_5.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_1.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </rect>
 <line id="giro9" x1="248.9" y1="237.1" x2="248.9" y2="244.3" fill="none" stroke="#fff" stroke-miterlimit="10" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_5.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_1.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </line>
 <line id="giro10" x1="252.6" y1="240.7" x2="245.2" y2="240.7" fill="none" stroke="#fff" stroke-miterlimit="10" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_5.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_1.mouseover;btn_6.mouseover" fill="freeze" repeatCount="1"/>
 </line>
 </a>
 </g>
 <g id="content_6">
 <text class="cont_1" transform="translate(183 137.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 6
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_6.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_1.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 157.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 6
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_6.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_1.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 167.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 6
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_6.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_1.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 177.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 6
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_6.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_1.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <text class="cont_1" transform="translate(183 187.6) scale(1.04 1)" font-size="11.6" fill="#fff" opacity="0" font-family="Sailec">
 Lorem ipsum Lorem ipsum 6
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_6.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_1.mouseover" fill="freeze" repeatCount="1"/>
 </text>
 <a xlink:href="http://www.google.com">
 <rect x="231.5" y="232.7" width="37.1" height="16.06" rx="8" ry="8" fill="#fff" fill-opacity="0" stroke-miterlimit="10" stroke-width="2" stroke="url(#m)" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_6.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_1.mouseover" fill="freeze" repeatCount="1"/>
 </rect>
 <line id="giro11" x1="248.9" y1="237.1" x2="248.9" y2="244.3" fill="none" stroke="#fff" stroke-miterlimit="10" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_6.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_1.mouseover" fill="freeze" repeatCount="1"/>
 </line>
 <line id="giro12" x1="252.6" y1="240.7" x2="245.2" y2="240.7" fill="none" stroke="#fff" stroke-miterlimit="10" opacity="0">
 <animate attributeName="opacity" to="1" dur="0.3s" begin="btn_6.mouseover" fill="freeze" repeatCount="1"/>
 <animate attributeName="opacity" to="0" dur="0.3s" begin="btn_2.mouseover;btn_3.mouseover;btn_4.mouseover;btn_5.mouseover;btn_1.mouseover" fill="freeze" repeatCount="1"/>
 </line>
 </a>
 </g>
 </svg>

</section>
