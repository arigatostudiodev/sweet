@php
  $imgPos = get_sub_field('imagen_pos');
  $pos = '';

  if( $imgPos == 'Centrada' ){
    $pos = 'justify-content-md-center';
  }elseif( $imgPos == 'Derecha' ){
    $pos = 'justify-content-md-end';
  }

  $imgPrioridad = get_sub_field('prioridad');
  $textClass = 'col-md-5 col-lg-4';
  $imgClass ='col-md-7 col-lg-6';

  $margin = get_sub_field('sin_margen');
  if($margin){
    $textClass = 'offset-lg-1 col-md-5 col-lg-4';
    $imgClass ='col-md-7 col-lg-7 text-right';
  }


  if( $imgPrioridad == 'Texto' ){
    $textClass = 'col-md-7 col-lg-6';
    $imgClass ='col-md-5 col-lg-4';
  }elseif( $imgPrioridad == 'Ambas' ){
    $textClass = 'col-md-6 col-lg-5';
    $imgClass ='col-md-6 col-lg-5';
  }
  $hasCaption = get_sub_field('incluir_etiqueta');

  $multiColumna = get_sub_field('bloque_multicolumna');
  $columnnas = get_sub_field('cantidad_de_columnas');

  $imageH = 'maxh-100';
  $columnReverse = 'flex-column-reverse';
  if($multiColumna){
    $imageH = 'maxh-70';
    $columnReverse = '';
  }

  $noMostrar = get_sub_field('no_mostrar');

  $hideClass = 'd-flex';

  if($noMostrar){
    $hideClass = 'd-none d-md-flex';
    $columnReverse = '';
  }
@endphp
<div class="container-fluid d-flex">
  <div class="row align-items-center justify-content-center flex-column {{$columnReverse}} flex-md-row mh-100vh">
    <div class="{{$textClass}}">
      <div class="section-content">
        <div class="cards-transparent no-bg">
          <div class="inner-card">
            @php the_sub_field('contenido') @endphp
          </div>
          @if($hasCaption)
            <div class="float-caption left-caption d-flex gradiente-11 align-items-center">
              <div class="caption-content">
                @php the_sub_field('etiqueta') @endphp
              </div>
              <div class="caption-action">
                <a href="{{the_sub_field('accion_etiqueta')}}" target="_blank" class="action-btn-caption"> @php the_sub_field('titulo_etiqueta') @endphp</a>
              </div>
            </div>
          @endif
        </div>
      </div>
    </div>
    <div class="{{ $imgClass }} {{ $pos }} {{$hideClass}}">
      @php $image = get_sub_field('imagen') @endphp
      @if( !empty($image) )
        <div class="section-img">
          <img src="{{ $image['url'] }} ?>" alt="{{ $image['alt'] }}" class="img-fluid {{$imageH}}"/>
        </div>
      @endif
    </div>
    @if($multiColumna)
    <div class="col-md-10">
      <div class="section-content text-col-{{$columnnas}}">
        @php the_sub_field('contenido_multicolumnas') @endphp
      </div>
    </div>
    @endif
  </div>
</div>
