<div class="wrap-animtacion-section flex-grow-1 d-flex align-items-md-center  align-items-start">
  <div class="container">
    <div class="row align-items-center justify-content-md-end mh-md-100vh">
      <div class="col-md-6">
        <div class="section-content">
          @php the_sub_field('contenido') @endphp
        </div>
      </div>
    </div>
  </div>
  <div class="section-animation-l d-none d-md-block">
      <img src="@asset('images/oval-1.svg')" class='img-fluid' alt='Sweet Analitycs'>
  </div>
</div>
