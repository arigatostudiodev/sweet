<footer class="content-info gradiente-4 position-relative">

  <div class="footer-nav">

    <div class="container-fluid">

      <div class="row d-flex align-items-center justify-content-center">

        <div class="col-md-5 col-lg-6">

          <div class="logo-footer">
            <img src="@asset('images/logo-footer.png')" class='img-fluid white-logo' alt='Sweet Analitycs'>
          </div>

        </div>

        <div class="col-md-7 col-lg-4">
          <div class="newsletter">
            <h4 class="font-heavy">¿Quieres aprender?</h4>
            <form class='footer-form'>

              <div class="d-flex justify-content-end flex-column flex-sm-row">

                <div class="flex-grow-1">

                  <fieldset class="form-group">
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Tu correo electrónico">
                  </fieldset>

                  <!--<fieldset class="form-group">
                    <input type="checkbox" id="checkbox-1-1" class="regular-checkbox" /><label for="checkbox-1-1"></label>
                    <span class="politicas">Acepto la politica de privacidad</span>
                  </fieldset>-->

                </div>

                <div class="">

                  <button type="submit" class="btn btn-white">Síguenos</button>

                </div>

              </div>

            </form>
          </div>
        </div>

      </div>

    </div>

  </div>

  <div class="nav-copyrigths">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-6 col-lg-4">
          @if (has_nav_menu('footer_navigation'))
            {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'footer-nav d-flex justify-content-between', 'container_class'=>'footer-nav-container']) !!}
          @endif
        </div>
        <div class="col-md-6 col-lg-6">
          <div class="copyrigths politicas d-flex justify-content-end">
            <p>Sweet Analytics © 2018 <span class="terminos-links">Privacidad / Cookies</span></p>
          </div>
        </div>
      </div>

    </div>
  </div>

</footer>
