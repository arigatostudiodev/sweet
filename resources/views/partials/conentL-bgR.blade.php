@php $fondo = get_sub_field('fondo'); @endphp
<div class="bg-wrapper {{ $fondo }}">
  <div class="container-fluid d-flex">
    <div class="row justify-content-center">
      <div class="col-lg-10">
        <div class="row align-items-center mh-100vh">
          <div class="col-md-6">
            <div class="section-content">
              <div class="cards-transparent no-bg">
                <div class="inner-card">
                  @php the_sub_field('contenido') @endphp
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @php
    $image = get_sub_field('imagen_de_fondo');
    $bgPos = get_sub_field('fondo_pos');
  @endphp
  @if( !empty(imagen_de_fondo) )
    <div class="bg-img bg-pos-{{ $bgPos }}">
      <img src="{{ $image['url'] }} ?>" alt="{{ $image['alt'] }}" class="img-fluid"/>
    </div>
  @endif
</div>
