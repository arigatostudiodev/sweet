@php
  $categories = get_categories( array(
    'orderby' => 'name',
    'parent'  => 0,
    'hide_empty' => 0,
  ) );
@endphp


@if($categories)
  <section class="blog-cat-menu">
    <div class="container-fluid">
      <div class="row align-items-center justify-content-center">
        <div class="col-md-10">
          <ul class="cat-nav d-flex align-items-md-center justify-content-center justify-content-between">
            @foreach ( $categories as $category )
              @if($category->slug !== "uncategorized")
                <li class="cat-nav-item">
                  <a href="{{ esc_url( get_category_link( $category->term_id ) )}}">{{ $category->name }}</a>
                </li>
              @endif
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </section>
@endif

@if(have_posts())
  @php $count = 0; $countSimple = 1; @endphp
  <section class='blog-page-wrapper'>
    <div class="container-fluid">
      <div class="row align-items-center justify-content-center">
        <div class="col-lg-10">
          <div class="row inner-post">
            @while(have_posts()) @php the_post() @endphp
              @php
                $count++;
                $partial = 'partials.blog-card-simple';
                $bg = 'background-white';
                if( $count == 2 or $count == 14){
                  $partial = 'partials.blog-card-wide';
                  if( $count == 14 ){
                    $count = 0;
                  }
                  $bg.=" shadow";
                }elseif( $count == 3 or $count == 6 or $count == 13 ){
                  $partial = 'partials.blog-card-bg';
                  $bg = 'gradiente-5';
                  if( $count == 6 ){
                    $bg = 'gradiente-1';
                  }elseif( $count == 13 ){
                    $bg = 'gradiente-3';
                  }
                }else{
                  $countSimple++;
                  $bg.= " shadow";
                  if (0 == $countSimple % 2) {
                    //$bg.= " shadow";
                  }
                }


              @endphp

              @include($partial,['bg' => $bg])

            @endWhile
          </div>
          @php
            global $wp_query;
            $pages =  $wp_query->max_num_pages;
          @endphp

          @if( $pages >1 )
            <div class="load-more-blog-wrapper">
              <a href="javascript:void(0)" class="a-btn load-more-blog" data-next="2" data-max="{{$pages}}" >Cargar más</a>
            </div>
          @endif
        </div>
      </div>
    </div>
  </section>
@endIf
