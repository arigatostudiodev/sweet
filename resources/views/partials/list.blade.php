<div class="items-list-sweet">
  <div class="row">
    @if( have_rows('items_a_listar') )
      @while ( have_rows('items_a_listar') ) @php the_row(); @endphp
       <div class="col-md-4">
         <div class="inner-item-sweet d-flex">
           <div class="item-image">
             @php
              $image = get_sub_field('imagen');
              $size = 'list-thumb';
              $thumb = $image['sizes'][ $size ];
             @endphp
             @if( !empty($image) )
               <div class="section-img">
                 <img src="{{ $thumb }} ?>" alt="{{ $image['alt'] }}" class="img-fluid rounded-circle"/>
               </div>
             @endif
           </div>
           <div class="item-content">
             <h5>@php the_sub_field('titulo'); @endphp</h5>
             @php the_sub_field('contenido'); @endphp
           </div>
         </div>
       </div>
      @endwhile
    @endif
  </div>
</div>
