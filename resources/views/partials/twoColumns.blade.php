@php
  $hasCaption = get_sub_field('incluir_etiqueta');

  $multiColumna = get_sub_field('bloque_multicolumna');
  $columnnas = get_sub_field('cantidad_de_columnas');
@endphp

<div class="container-fluid d-flex">
  <div class="row align-items-center justify-content-center mh-100vh">
    <div class="col-sm-6">
      <div class="section-content">
        <div class="cards-transparent no-bg">
          <div class="inner-card">
            @php the_sub_field('contenido') @endphp
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-6 col-lg-4 d-none d-sm-flex justify-content-end">
      <div class="section-content contenido-derecha">
        <div class="cards-transparent no-bg">
          <div class="inner-card">
            @php the_sub_field('contenido_derecha') @endphp
          </div>
          @if($hasCaption)
            <div class="float-caption left-caption d-flex gradiente-11 align-items-center">
              <div class="caption-content">
                @php the_sub_field('etiqueta') @endphp
              </div>
              <div class="caption-action">
                <a href="{{the_sub_field('accion_etiqueta')}}" target="_blank" class="action-btn-caption"> @php the_sub_field('titulo_etiqueta') @endphp</a>
              </div>
            </div>
          @endif
        </div>
      </div>
    </div>
    @if($multiColumna)
    <div class="col-md-10">
      <div class="section-content text-col-{{$columnnas}}">
        @php the_sub_field('contenido_multicolumnas') @endphp
      </div>
    </div>
    @endif
  </div>
</div>
