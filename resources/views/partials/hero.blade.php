@if( have_rows('hero_image') )
  @while ( have_rows('hero_image') ) @php the_row(); @endphp
    @php $image = get_sub_field('imagen') @endphp
    @if( !empty($image) )
      <style>
        .hero{
          background: url('{{ $image['url'] }}') no-repeat center center;
          background-size: cover;
        }
      </style>
    @endif
    <div class="hero mh-100vh d-flex align-items-center justify-content-center">
      <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-md-10">
            <div class="hero-content">
              @php the_sub_field('contenido') @endphp
            </div>
          </div>
        </div>
      </div>
    </div>
  @endwhile
@endif
