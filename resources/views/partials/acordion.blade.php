
<div class="container-fluid accordion-Sweet">
  <div class="row align-items-center justify-content-center mh-100vh">
    <div class="col-md-10">
      <h4 class="titulo-subrayado">{{$titulo}}</h4>
      <div class="accordion" id="accordionSweet">
        @if( have_rows('faqs') )
          @php $count = 0; $show = 'show'; $collapsed = ''; @endphp
          @while ( have_rows('faqs') ) @php the_row(); @endphp
            <div class="card">
              <div class="card-header" id="heading-{{$count}}">
                <h4 class="mb-0">
                  <button class="btn btn-link {{$collapsed}}" type="button" data-toggle="collapse" data-target="#collapse-{{$count}}" aria-expanded="true" aria-controls="collapse-{{$count}}">
                    @php the_sub_field('titulo'); @endphp
                  </button>
                </h4>
              </div>

              <div id="collapse-{{$count}}" class="collapse {{$show}}" aria-labelledby="headingOne" data-parent="#accordionSweet">
                <div class="card-body">
                  @php the_sub_field('faq'); @endphp
                </div>
              </div>
            </div>
            @php $count++; $show = ''; $collapsed = 'collapsed'; @endphp
          @endwhile
        @endif
      </div>

    </div>
  </div>
</div>
