<header class="banner fixed-top">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-lg-10">
        <div class="menu-wrapper d-flex align-items-center">
          <div class="menu-logo flex-grow-1">
            <a class="brand" href="{{ home_url('/') }}">
              <object data="@asset('images/logo.svg')" type="image/svg+xml">
                <img src="@asset('images/logo.svg')" class='img-fluid white-logo' alt='Sweet Analitycs'>
              </object>

              <img src="@asset('images/logo-black.svg')" class='img-fluid black-logo' alt='Sweet Analitycs'>
            </a>
          </div>

          <nav class="nav-primary d-flex justify-content-end navbar navbar-expand-lg navbar-custom" id='primary-nav'>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            @if (has_nav_menu('primary_navigation'))
              {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav d-flex align-items-end align-items-md-center justify-content-end flex-column flex-lg-row', 'container_class'=>'collapse navbar-collapse', 'container_id'=>'navbarSupportedContent']) !!}
            @endif
          </nav>
        </div>
      </div>
      </div>
    </div>
</header>
