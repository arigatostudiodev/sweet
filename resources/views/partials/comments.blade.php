@php
if (post_password_required()) {
  return;
}
@endphp

<section id="comments" class="comments">
  @if (have_comments())
    <div class="comments-headers">
      <h4 class="text-center">Comentarios <span class="comments-count">{{ get_comments_number() }}</span></h4>
      <a href="#comment" id="comment-anchor"><img src="@asset('images/comments.png')" class='img-fluid' alt='Sweet Analitycs - comments'></a>
    </div>

    <div class="comment-list">
      {!! wp_list_comments(['style' => 'div', 'short_ping' => true, 'walker' => new App\comments_reloaded()]) !!}
    </div>

    @if (get_comment_pages_count() > 1 && get_option('page_comments'))
      <nav>
        <ul class="pager">
          @if (get_previous_comments_link())
            <li class="previous">@php previous_comments_link(__('&larr; Older comments', 'sage')) @endphp</li>
          @endif
          @if (get_next_comments_link())
            <li class="next">@php next_comments_link(__('Newer comments &rarr;', 'sage')) @endphp</li>
          @endif
        </ul>
      </nav>
    @endif
  @endif

  @if (!comments_open() && get_comments_number() != '0' && post_type_supports(get_post_type(), 'comments'))
    <div class="alert alert-warning">
      {{ __('Comments are closed.', 'sage') }}
    </div>
  @endif

  @php comment_form() @endphp
</section>
