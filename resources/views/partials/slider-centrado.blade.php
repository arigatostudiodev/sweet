<section class="section-item wait">
  @if( have_rows('laminas_centradas') )
    @while ( have_rows('laminas_centradas') ) @php the_row(); @endphp
      @php $fondo = get_sub_field('fondo'); @endphp
      <div class="slider-centrado-wrapper {{ $fondo }} position-relative mh-100vh d-flex align-items-center">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="col-md-10">
              <h3>@php the_sub_field('titulo') @endphp</h3>
            </div>
          </div>
          <div class="container-fluid">
            <div class="row justify-content-center">
              <div class="col-md-10">
                <div class="slider-centrado">
                  @if( have_rows('laminas_internas') )
                    @while ( have_rows('laminas_internas') ) @php the_row(); @endphp
                      <div class="slider-centrado-item">
                        <div class="cards-transparent">
                          <div class="inner-card">
                            @php the_sub_field('contenido') @endphp
                          </div>
                        </div>
                      </div>
                    @endwhile
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    @endwhile
  @endif
</section>
