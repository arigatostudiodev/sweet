<article @php post_class('silgle-blog') @endphp>
  <header class="fondo-foto-11 mh-100vh d-flex align-items-center justify-content-center text-center">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-10">
          @php $categories = get_the_category(); @endphp
          <div class="post-cat text-center">
            <a href="{{esc_url( get_category_link( $categories[0]->term_id ) )}}" class="post-cat-link">{{$categories[0]->name}}</a>
          </div>
          <h1 class="entry-title text-center">{{ get_the_title() }}</h1>
          @include('partials/entry-meta')
        </div>
      </div>
    </div>
  </header>

  <div class="entry-content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-lg-10">
          @php the_content() @endphp
        </div>
      </div>
    </div>
  </div>

  <footer>
    @php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]') @endphp
  </footer>

  <!-- <div class="fb-comments-blog">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-lg-10">
          <div class="fb-comments" data-href="{{ the_permalink() }}" data-width="1440" data-numposts="5"></div>
        </div>
      </div>
    </div>
  </div> -->
  <div class="fb-comments-blog">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-lg-10">
          @php comments_template('/partials/comments.blade.php') @endphp
        </div>
      </div>
    </div>

  </div>

</article>

<div class="post-populares gradiente-7">
  @php
  $posts = wp_most_popular_get_popular( array( 'limit' => 3, 'post_type' => 'post', 'range' => 'all_time' ) );
  global $post;
  @endphp
  @if ( count( $posts ) > 0 )
    @php $count = 0; @endphp
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-lg-10">
          <h3 class="text-center"><span class="underline-middle">Post Destacados</span></h3>
          <div class="wrap-popular-post-cards">
            <div class="row">
              @foreach ( $posts as $post )
                @php setup_postdata( $post ); @endphp
                @php
            			$count++;
            			$partial = 'partials.blog-card-simple';
            			$bg = 'background-white shadow';
            			if( $count == 3){
            				$partial = 'partials.blog-card-wide';
            			}elseif( $count == 2 ){
            				$partial = 'partials.blog-card-bg';
            				$bg = 'gradiente-5';
            			}
            		@endphp

                        @include($partial,['bg' => $bg])

              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  @else

  @endif
</div>
