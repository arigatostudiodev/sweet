@php
  $conoce = get_sub_field('conoce');
  $fondo = $conoce[0]['fondo'];
@endphp
<section id="conoce" class="wait section-conoce">

  <div id="bg-conoce" data-bg="{{$fondo}}" class="mh-100vh d-md-flex align-items-center justify-content-center {{$fondo}} d-none ">
    <div class="circle-conoce">

      <div class="contenido-conoce-wrapper d-flex align-items-center justify-content-center w-100 h-100">


        @if( have_rows('conoce') )
          @php $count = 1; @endphp
          @while ( have_rows('conoce') ) @php the_row(); @endphp
            <div class="contenido-conoce contenido-conoce-{{$count}}">
              @php the_sub_field('contenido') @endphp
            </div>
            @php $count++; @endphp
          @endwhile

        @endif

      </div>

      @if( have_rows('conoce') )
        @php $count = 1; @endphp

        @while ( have_rows('conoce') ) @php the_row(); @endphp
          @php $bg = get_sub_field('fondo'); @endphp
          <div class="click-conoce conoce-item conoce-item-{{$count}}" data-id="{{$count}}" data-bg="{{$bg}}"><span></span></div>
          <div class="click-conoce label-item label-item-{{$count}}" data-id="{{$count}}" data-bg="{{$bg}}">@php the_sub_field('titulo') @endphp</div>
          @php $count++; @endphp
        @endwhile

      @endif

      <div class="animacion-item"></div>


    </div>
  </div>

  <div id="carousel-conoce" class="carousel slide carousel-fade h-100 d-md-none" data-ride="carousel" data-interval="false">

    <ol class="carousel-indicators Bullets">
      @if( count($conoce) > 1 )
        @php $active = 'active'; @endphp
        @for ($i = 0; $i < count($conoce); $i++)
          <li data-target="#carousel-conoce" data-slide-to="{{ $i }}" class="{{ $active }} bullets"></li>
          @php $active = ''; @endphp
        @endfor
      @endif
    </ol>

    <div class="carousel-inner h-100">
      @if( have_rows('conoce') )

        @php $active = 'active'; @endphp

        @while ( have_rows('conoce') ) @php the_row(); @endphp

          @php $fondo = get_sub_field('fondo'); @endphp

          <div class="carousel-item {{ $active }} {{ $fondo }} h-100">
            @include('partials.contentL')
          </div>

          @php $active = ''; @endphp
        @endwhile

      @endif
    </div>

  </div>
</section>
