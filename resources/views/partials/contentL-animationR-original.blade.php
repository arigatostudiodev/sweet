<div class="wrap-animtacion-section">
  <div class="container d-flex">
    <div class="row align-items-center mh-100vh">
      <div class="col-lg-6 col-md-8">
        <div class="section-content">
          @php the_sub_field('contenido') @endphp
        </div>
      </div>
    </div>
  </div>
  <div class="section-animation-r d-none d-md-block">
      <img src="@asset('images/oval-1.svg')" class='img-fluid' alt='Sweet Analitycs'>
  </div>
</div>
