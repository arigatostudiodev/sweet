<div class="container-fluid">
  <div class="row align-items-center justify-content-center mh-100vh">
    <div class="col-lg-10">
      <div class="section-content">
        <div class="row align-items-center">
          <div class="col-md-6">
              <h2>@php the_sub_field('titulo') @endphp</h2>
          </div>
          <div class="col-md-6">
            <div class="cards-transparent no-bg">
              <div class="inner-card">
                @php the_sub_field('contenido') @endphp
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
