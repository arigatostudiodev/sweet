@php $categories = get_the_category(); @endphp

<div class="col-md-3 blog-card-item">
  <div class="blog-card blog-card-simple {{ $bg }} blog-cat-{{$categories[0]->slug}}" data-url="{{the_permalink()}}">

    <div class="blog-card-cat">
      <span>{{$categories[0]->name}}</span>
    </div>

    @if(has_post_thumbnail)
      <div class="blog-card-thumb d-flex align-items-center justify-content-center">
        {{ the_post_thumbnail('category-thumb', ['class' => 'img-fluid', 'title' => get_the_title() ]) }}
      </div>
    @endIf

    <div class="blog-card-info">
      <div class="blog-card-title">
        <h6 class="font-heavy">{{ the_title() }}</h6>
      </div>

      <div class="blog-card-excerpt">
        {{ the_excerpt() }}
      </div>
    </div>

  </div>
</div>
