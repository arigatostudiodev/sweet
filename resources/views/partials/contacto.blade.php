@if( have_rows('contacto') )
  @while ( have_rows('contacto') ) @php the_row(); @endphp
    @php  $fondo = get_sub_field('fondo'); @endphp
    <section id="contacto" class="wait section-contact section-item contacto">
      <div class="{{$fondo}}">
        <div class="container-fluid">
          <div class="row align-items-center justify-content-center">
            <div class="col-lg-10">
              <div class="row mh-100vh align-items-center justify-content-center ">


                    <div class="col-lg-6 col-sm-6">
                      <div class="formulario">
                        <div class="header-formulario">
                          @php the_sub_field('encabezado'); @endphp
                        </div>

                        <div class="formulario">

                            @php
                              $formulario = get_sub_field('formulario');
                              echo do_shortcode( $formulario );
                            @endphp

                        </div>

                      </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                      <div class="direccion">
                        @php the_sub_field('direccion'); @endphp
                      </div>
                    </div>


              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endwhile
@endif
