@php $columnnas = get_sub_field('cantidad_de_columnas'); $listar = get_sub_field('listar_items'); @endphp
<div class="container-fluid d-flex">
  <div class="row align-items-center justify-content-center mh-100vh">
    <div class="col-md-10">
      <div class="section-content content-ppal">
        @php the_sub_field('contenido') @endphp
      </div>

      <div class="section-content text-col-{{$columnnas}}">
        @php the_sub_field('contenido_multicolumnas') @endphp
      </div>

      @if($listar)
        @include('partials.list')
      @endif
    </div>
  </div>
</div>
