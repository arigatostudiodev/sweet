@php
  $wrapperClasss = 'mh-100vh';
  if($flag_titulo){
    $wrapperClasss = 'mh-60vh';
  }
@endphp

<div class="container-fluid">
  <div class="row align-items-center justify-content-center {{$wrapperClasss}}">
    <div class="col-lg-10">
      <div class="section-content">
        <div class="row justify-content-end">
          <div class="col-sm-10 col-md-8 col-lg-6">
            <div class="cards-transparent no-bg">
              <div class="inner-card">
                @php the_sub_field('contenido') @endphp
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
