@php $categories = get_the_category(); @endphp

<div class="col-md-3 blog-card-item">
  <div class="blog-card blog-card-bg {{ $bg }} blog-cat-{{$categories[0]->slug}}" data-url="{{the_permalink()}}">

    <div class="blog-card-cat text-center">
      {{$categories[0]->name}}
    </div>

    <div class="blog-card-info">
      <div class="blog-card-title">
        <h5 class="text-center font-heavy">{{ the_title() }}</h5>
      </div>

      <div class="blog-card-excerpt text-center">
        {{ the_excerpt() }}
      </div>
    </div>

    <div class="blog-card-view text-center">
      <a href="{{ the_permalink() }}" class="a-btn">Ver Más</a>
    </div>

  </div>
</div>
