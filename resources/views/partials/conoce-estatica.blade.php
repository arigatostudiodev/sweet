<section id="conoce" class="wait section-conoce">
  <div class="mh-100vh d-flex align-items-center justify-content-center fondo-foto-6">
    <div class="circle-conoce">
      <div class="click-conoce conoce-item conoce-item-1" data-id="1"><span></span></div>
      <div class="click-conoce conoce-item conoce-item-2" data-id="2"><span></span></div>
      <div class="click-conoce conoce-item conoce-item-3" data-id="3"><span></span></div>
      <div class="click-conoce conoce-item conoce-item-4" data-id="4"><span></span></div>
      <div class="click-conoce conoce-item conoce-item-5" data-id="5"><span></span></div>
      <div class="click-conoce conoce-item conoce-item-6" data-id="6"><span></span></div>

      <div class="animacion-item"></div>

      <div class="click-conoce label-item label-item-1" data-id="1"><p><strong>Single Customer View</strong> (SCV)</p></div>
      <div class="click-conoce label-item label-item-2" data-id="2"><p><strong>RFM</strong></p></div>
      <div class="click-conoce label-item label-item-3" data-id="3"><p><strong>RFPD</strong></p></div>
      <div class="click-conoce label-item label-item-4" data-id="4"><p><strong>Real Time</strong> (rt)</p></div>
      <div class="click-conoce label-item label-item-5" data-id="5"><p><strong>Single Customer View </strong> (sc)</p></div>
      <div class="click-conoce label-item label-item-6" data-id="6"><p><strong>User Journey</strong> (UJ)</p></div>

      <div class="contenido-conoce-wrapper d-flex align-items-center justify-content-center w-100 h-100">
        <div class="contenido-conoce contenido-conoce-1">
          <h2>Conoce</br>Sweet Analitycs</h2>
          <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. sem accumsan ipsum velit. Nam nec tellus.</p>
        </div>
        <div class="contenido-conoce contenido-conoce-2">
          <h2>Conoce</br>Sweet Analitycs 2</h2>
          <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. sem accumsan ipsum velit. Nam nec tellus.</p>
        </div>
        <div class="contenido-conoce contenido-conoce-3">
          <h2>Conoce</br>Sweet Analitycs 3</h2>
          <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. sem accumsan ipsum velit. Nam nec tellus.</p>
        </div>
        <div class="contenido-conoce contenido-conoce-4">
          <h2>Conoce</br>Sweet Analitycs 4</h2>
          <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. sem accumsan ipsum velit. Nam nec tellus.</p>
        </div>
        <div class="contenido-conoce contenido-conoce-5">
          <h2>Conoce</br>Sweet Analitycs 5</h2>
          <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. sem accumsan ipsum velit. Nam nec tellus.</p>
        </div>
        <div class="contenido-conoce contenido-conoce-6">
          <h2>Conoce</br>Sweet Analitycs 6</h2>
          <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. sem accumsan ipsum velit. Nam nec tellus.</p>
        </div>
      </div>

    </div>
  </div>
</section>
