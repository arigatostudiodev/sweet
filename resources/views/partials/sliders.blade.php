@php
  $titulo = get_sub_field('nombre_seccion');
  $id = strtolower($titulo);
  $rows = get_sub_field('laminas');
  $countLaminas = count( $rows );
  $active = 'active';

  $tipo = get_sub_field('tipo');
  $indicatorClass = 'numbers';
  $bulletsPos = get_sub_field('bullets_pos');
  $countIndicator = 1;
  if( $tipo == 'Bullets'){
    $indicatorClass = 'bullets';
  }
  if($bulletsPos == 'Centro'){
    $indicatorClass .= ' bullets-center';
  }elseif($bulletsPos == 'Derecha'){
      $indicatorClass .= ' bullets-end';
  }
@endphp
<section class="section-item slider section-{{ $id }} wait">

  <div id="carousel-{{ $id }}" class="carousel slide carousel-fade h-100" data-ride="carousel" data-interval="false">

    <ol class="carousel-indicators {{ $indicatorClass }}">
      @if( $countLaminas > 1 )
        @foreach($rows as $key => $row)
          <li data-target="#carousel-{{ $id }}" data-slide-to="{{ $key }}" class="{{ $active }} {{ $indicatorClass }}">
            @if( $tipo !== 'Bullets' )
              <span class='count-bullets'>{{$countIndicator}}</span>
            @endif
          </li>
          @php $active = ''; $countIndicator++; @endphp
        @endforeach
      @endif
    </ol>

    <div class="carousel-inner h-100">
      @if( have_rows('laminas') )

        @php $active = 'active'; @endphp

        @while ( have_rows('laminas') ) @php the_row(); @endphp

          @php
            $type = get_sub_field('formato');
            $fondo = get_sub_field('fondo');
            $partialArray = App\getPartial($type, $fondo);
            $partial = $partialArray['partial'];
            $fondo = $partialArray['fondo'];
            $margin = get_sub_field('sin_margen');
            if($margin){
              $marginClass = "no-margin-img";
            }
          @endphp

          <div class="carousel-item {{ $active }} {{ $fondo }} {{$marginClass}} h-100">
            @include($partial)
          </div>

          @php $active = ''; @endphp

        @endwhile

      @endif
    </div>

  </div>

</section>
