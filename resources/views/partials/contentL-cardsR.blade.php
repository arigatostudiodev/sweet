<div class="container d-flex">
  <div class="row align-items-center mh-lg-100vh">
    <div class="col-lg-6">
      <div class="section-content">
        <div class="cards-transparent no-bg">
          <div class="inner-card">
            @php the_sub_field('contenido') @endphp
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="section-card-wrapper d-flex flex-column align-items-end ml-auto">

        @if( have_rows('tarjetas') )
          @while ( have_rows('tarjetas') ) @php the_row(); @endphp
            <div class="section-card section-card-title w-100">
              @php the_sub_field('titulo_de_las_tarjetas') @endphp
            </div>
            @if( have_rows('infomarcion') )
              @while ( have_rows('infomarcion') ) @php the_row(); @endphp
                <div class="section-card w-100">
                  <div class="section-card-header d-flex align-items-center">
                    <p class='section-card-header-title flex-grow-1'>@php the_sub_field('titulo') @endphp</p>
                    <p class='section-card-header-date flex-shrink-1'>@php the_sub_field('fecha') @endphp</p>
                  </div>
                  <div class="section-card-content">
                    @php the_sub_field('contenido') @endphp
                  </div>
                </div>
              @endwhile
            @endif
          @endwhile
        @endif

      </div>
    </div>
  </div>
</div>
