@if( have_rows('aliados') )
  @while ( have_rows('aliados') ) @php the_row(); @endphp
    @php $fondo = get_sub_field('fondo'); @endphp
    <section class="partners-wrapper {{ $fondo }} position-relative section-item">
      <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-11 col-lg-10">
              <div class="partners">

                  @if( have_rows('logos') )
                    @while ( have_rows('logos') ) @php the_row(); @endphp
                      @php $url = get_sub_field('url'); @endphp
                      @if( !empty($url) )
                        <a href="{{ $url }}">
                      @endif

                      @php $image = get_sub_field('logo') @endphp
                      @if( !empty($image) )
                        <img src="{{ $image['url'] }} ?>" alt="{{ $image['alt'] }}" class="img-fluid"/>
                      @endif

                      @if( !empty($url) )
                        </a>
                      @endif
                    @endwhile
                  @endif

              </div>
            </div>
          </div>
        </div>
    </section>
@endwhile
@endif
