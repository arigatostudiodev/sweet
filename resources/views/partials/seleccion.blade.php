@php $seleccion = get_sub_field('seleccion');  @endphp

@if(!empty($seleccion))
  <div class="container-fluid d-flex">
    <div class="row align-items-end align-items-md-center justify-content-center mh-100vh">
      <div class="col-md-5 col-lg-4">
        <h3 class="titulo-subrayado">{{$titulo}}</h3>
        @if( !empty($seleccion['opciones']) )
          @php $count = 0; $active = "active"; @endphp
          <ul class="nav nav-tabs nav-seleccion" id="tabSeleccion" role="tablist">
          @foreach($seleccion['opciones'] as $opcion)
            <li class="nav-item">
              <a class="nav-link {{$active}}" id="tab{{$count}}" data-toggle="tab" href="#tab-{{$count}}" role="tab" aria-controls="tab-{{$count}}" aria-selected="true" >{{ $opcion['nombre'] }}</a>
            </li>
            @php $count++; $active = ""; @endphp
          @endforeach
          </ul>
        @endif
      </div>
      <div class="col-md-7 col-lg-6">
        @if( !empty($seleccion['opciones']) )
          @php $count = 0; $active = "active"; @endphp
          <div class="tab-content" id="SeleccionTabContent">
          @foreach($seleccion['opciones'] as $opcion)
            <div class="tab-pane fade show {{$active}}" id="tab-{{$count}}" role="tabpanel" aria-labelledby="tab{{$count}}">
              <div class="cards-transparent">
                <div class="inner-card">
                  @php echo apply_filters( 'the_content', $opcion['contenido'] ); @endphp
                </div>
              </div>
            </div>
            @php $count++; $active = ""; @endphp
          @endforeach
          </div>
        @endif
      </div>
    </div>
  </div>
@endif
