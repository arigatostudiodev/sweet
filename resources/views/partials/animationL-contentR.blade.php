<div class="wrap-animtacion-section-full flex-grow-1 d-flex align-items-md-center  align-items-start">
  <div class="container-fluid">
    <div class="row align-items-center justify-content-md-center mh-md-100vh">

      <div class="col-md-7 col-lg-6">
        <img src="@asset('images/oval-1.svg')" class='img-fluid animation-h' alt='Sweet Analitycs'>
      </div>

      <div class="col-md-5 col-lg-4">
        <div class="section-content">
          <div class="cards-transparent no-bg">
            <div class="inner-card">
              @php the_sub_field('contenido') @endphp
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
