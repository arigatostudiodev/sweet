@php
  $titulo = get_sub_field('nombre_seccion');
  $id = strtolower($titulo);
@endphp
<section class="section-item section-{{ $id }} wait">
  @if( have_rows('seccion') )

    @while( have_rows('seccion') ) @php the_row() @endphp
      @php
        $type = get_sub_field('formato');
        $fondo = get_sub_field('fondo');
        $flag_titulo = get_sub_field('flag_titulo');
        $titulo = get_sub_field('titulo');
        $partialArray = App\getPartial($type, $fondo);
        $partial = $partialArray['partial'];
        $fondo = $partialArray['fondo'];

        if($flag_titulo){
          $wrapperClasss = 'mh-40vh';
        }

        $margin = get_sub_field('sin_margen');
        if($margin){
          $marginClass = "no-margin-img";
        }
      @endphp

      <div class="{{ $fondo }} d-flex flex-column  {{$marginClass}} mh-100vh">
        @if( $flag_titulo )
        <div class="container-fluid">
          <div class="row justify-content-center align-items-end {{ $wrapperClasss }}">
            <div class="col-lg-10">
              <h1 class="titulo-seccion">{{ $titulo }}</h1>
            </div>
          </div>
        </div>
        @endif

        @include($partial, ['titulo' => $titulo, 'flag_titulo' => $flag_titulo])

      </div>
    @endwhile

  @endif
</section>
