@php $categories = get_the_category(); @endphp

<div class="col-md-6 blog-card-item">
  <div class="blog-card blog-card-wide blog-cat-{{$categories[0]->slug}} {{ $bg }}" data-url="{{the_permalink()}}">

    <div class="row h-100">

      <div class="col-md-6">

        <div class="blog-card-cat">
          <span>{{$categories[0]->name}}</span>
        </div>

        <div class="blog-card-info">
          <div class="blog-card-title">
            <h5 class="font-heavy">{{ the_title() }}</h5>
          </div>

          <div class="blog-card-excerpt">
            {{ the_excerpt() }}
          </div>
        </div>

      </div>

      <div class="col-md-6 h-100 d-flex align-items-center justify-content-center">
        @if(has_post_thumbnail)
          <div class="blog-card--thumb">
            {{ the_post_thumbnail('category-thumb', ['class' => 'img-fluid', 'title' => get_the_title() ]) }}
          </div>
        @endIf
      </div>

    </div>
  </div>
</div>
