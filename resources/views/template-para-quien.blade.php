{{--
  Template Name: Para quien? Template
--}}

@extends('layouts.full')

@section('content')
  @while(have_posts()) @php the_post() @endphp

    @include('partials.bloque')

  @endwhile
@endsection
