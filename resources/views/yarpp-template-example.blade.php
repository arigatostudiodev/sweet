{{--
YARPP Template: Sweet
Author: Ramón Téllez
Description: Sweet YARPP template.
--}}

@if (have_posts())
	@php $count=0; @endphp
	@while (have_posts()) @endwhile
		@php the_post(); @endphp
		@php
			$count++;
			$partial = 'partials.blog-card-simple';
			$bg = 'bg-white shadow';
			if( $count = 3){
				$partial = 'partials.blog-card-wide';
			}elseif( $count == 2 ){
				$partial = 'partials.blog-card-bg';
				$bg = 'gradiente-5';
			}
		@endphp

		@include($partial,['bg' => $bg])
	@endwhile;
@else

@endif;
